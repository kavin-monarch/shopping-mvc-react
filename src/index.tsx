import React from 'react';
import ReactDOM from 'react-dom';
import '../src/styles/index.css';
import App from './ts/App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
