interface product_schema {
    product_id:number,
    product_image:string,
    product_name:string,
    product_price:number
}

interface cart_schema extends product_schema{
product_count:number
}


type cartObserver =(cart:number)=>void;