import {user} from '../Model/cart'
import cartManager from '../Model/observer';
export const Controller_modules =(()=>{
    const addToCart =(product:product_schema):void=>{
        user.addToCart(product);
        cartManager.updateCart();
    }

    const getLengthCart =():number=>{
        return user.getCart().length;
    }

    const getCart =():Array<cart_schema>=>{
        return user.getCart();
    }

    const add_reduce=(select:number,index:number):void=>{
        user.add_reduce(select,index);
    }

    const completeRemove=()=>{
        user.update([]);
        cartManager.updateCart();
    }

    const getTotal=():number=>{
        let total:number=0;
        getCart().map((item)=>{
            total+=item.product_price*item.product_count;
            return item;
        })
        return total;
    }
    
    return {
        addToCart:addToCart,
        getLengthCart:getLengthCart,
        getCart:getCart,
        add_reduce:add_reduce,
        completeRemove:completeRemove,
        getTotal:getTotal
    }

})();