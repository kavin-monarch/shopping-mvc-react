import { useState,useEffect} from 'react';
import Card from './components/card/Card';
import Header from './components/Header/Header';
import PopupTest from './components/Popup/PopupTest';
import {products} from './Model/products';
import { Controller_modules } from './Controller/controller';

function App() : JSX.Element {
  const [cart,setCart] =useState(Controller_modules.getLengthCart());
  const [product,setProduct]=useState(products);
  const [total,setTotal]=useState(0);
  const [pop,setPop]=useState(false);
  useEffect(() => {
    setCart(Controller_modules.getLengthCart());
  }, [total,cart,setCart])


  const changePop=():void=>{
    setPop(prev=>!prev);
    setTotal(Controller_modules.getTotal());
    setProduct(products);
  }

  return (
    <>
      <Header 
      cart={cart} 
      setCart={setCart} 
      changePop={changePop}
      setProduct={setProduct}
      />
      <div className="main">
        {
        product.length>0?
            product.map((items,index):JSX.Element=>{
              return (
              <Card 
              key={index} 
              product={items}
              cart={cart}
              setCart={setCart}
              />
              )
            }):<h1>No Result Found</h1>
        }
      </div>

      {pop?<PopupTest 
      pop={pop} 
      setPop={setPop}
      setCart={setCart}
      total={total}
      setTotal={setTotal}
      />:null}
    </>
  )
}

export default App
