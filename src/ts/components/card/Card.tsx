import {Controller_modules} from '../../Controller/controller';
import {useEffect} from 'react';
import cartManager from '../../Model/observer';

interface Props{
        product:product_schema,
        cart:number,
        setCart:(value:number)=>void
}
function Card(prop:Props):JSX.Element {
    const addToCart =():void=>{
        Controller_modules.addToCart(prop.product);
    }
    const observer:cartObserver=(cart1:number)=>{
        prop.setCart(cart1);
    }
    useEffect(() => {
        cartManager.attach(observer);
        return () => cartManager.detach(observer);
    },)
    
    return (
        <div className="card">
            <img src={prop.product.product_image} alt="" />
            <div className="card_bottom">
                <h1>{prop.product.product_name}</h1>
                <h1>₹{prop.product.product_price}</h1>
            </div>
            <input type="button" value="Add To Card" onClick={()=>addToCart()} />

        </div>
    )
}

export default Card;
