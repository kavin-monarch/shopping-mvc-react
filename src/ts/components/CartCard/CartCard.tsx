import { useState} from 'react'
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import {Controller_modules} from '../../Controller/controller';
import DeleteIcon from '@material-ui/icons/Delete';

interface Props{
    item:cart_schema,
    index:number,
    setCart1:(value:cart_schema[])=>void,
    setTotal:(value:number)=>void
}

function CartCard({item,index,setCart1,setTotal}:Props) {
    const [quantity,setQuantity]=useState(Controller_modules.getCart()[index].product_count)

    const add_reduce =(select:number):void=>{
            Controller_modules.add_reduce(select,index);
            if(Controller_modules.getCart()[index]!==undefined)
            setQuantity(Controller_modules.getCart()[index].product_count);
            setCart1(Controller_modules.getCart());
            setTotal(Controller_modules.getTotal());
    }
    return (
        <div className="cart_item">
            <h3>{item.product_name}</h3>
            <div className="cart_item_right">
                Price:{item.product_price}
                <AddCircleIcon onClick={()=>add_reduce(1)}/>
                <h4>{quantity}</h4>
                <RemoveCircleIcon onClick={()=>add_reduce(-1)}/>
                Total:{(item.product_price*quantity)}
                <DeleteIcon onClick={()=>add_reduce(0)}/>
            </div>
        </div>
    )
}

export default CartCard
