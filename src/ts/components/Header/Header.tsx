import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import {useRef,ChangeEvent} from 'react';
import {products} from '../../Model/products';
import BackspaceIcon from '@material-ui/icons/Backspace';


interface Props{
    cart:number,
    setCart:(value:number) => void,
    changePop:()=>void,
    setProduct:(value:Array<product_schema>)=>void,
}

function Header({cart,setCart,changePop,setProduct} : Props) :JSX.Element{
    const searchRef =useRef<HTMLInputElement>(null);
    
    const search =(e:ChangeEvent<HTMLInputElement>)=>{
        searching(e.target.value);
    }

    const searching=(v:string)=>{
        const result = products.filter((k) => {
            return k.product_name.toLowerCase().includes(v.toLowerCase());
        })
        setProduct(result);
    }
    return (
        <div className="header">
            <h2>Shopping Cart</h2>
            <div className="search__input" >
            <input 
            ref={searchRef} 
            type="text" name="" 
            id="" 
            className="search__input" 
            onChange={(e)=>search(e)}
            placeholder="Search Every Product in The Planet"
            />
            {searchRef.current!==null && searchRef.current.value.length>0?<BackspaceIcon />:null}
            </div>
            <div className="header_right" onClick={changePop}>
                <h3>Cart: ({cart})</h3>
                <ShoppingCartIcon/>
                <h3>Kavin</h3>
            </div>
        </div>
    )
}

export default Header