import {useRef,MouseEvent,useState,useEffect} from 'react'
import CartCard from '../CartCard/CartCard';
import {Controller_modules} from '../../Controller/controller';
import cartManager from '../../Model/observer';

interface Props{
    pop:boolean,
    setPop:(value:boolean)=>void,
    setCart:(value:number)=>void,
    total:number,
    setTotal:(value:number)=>void
}

function PopupTest({pop,setPop,setCart,total,setTotal}:Props):JSX.Element {
    const closeRef=useRef<HTMLDivElement>(null);
    const [cart1,setCart1]=useState(Controller_modules.getCart);
    const closePopUp =(e:MouseEvent<HTMLDivElement>):void=>{
        if(closeRef.current===e.target){
            setPop(!pop);
        }
    }
    const observer:cartObserver=(cart1:number)=>{
        setCart(cart1);
        setCart1(Controller_modules.getCart());
        setTotal(Controller_modules.getTotal());
        setPop(!pop);
    }
    const Checkout =():void=>{
        if(cart1.length>0){
            alert('Item purchaced RS:'+total);
            Controller_modules.completeRemove();
        }
    }
    useEffect(() => {
        cartManager.attach(observer);
        return () => cartManager.detach(observer);
    },)
    return (
            <div ref={closeRef} className="container" onClick={closePopUp}>
                <div className="form">
                   <h1>Your Cart</h1>
                   
                   {cart1.length>0?cart1.map((item,index):JSX.Element=>{
                       return(
                           <CartCard 
                           key={index} 
                           item={item} 
                           index={index} 
                           setTotal={setTotal}
                           setCart1={setCart1}/>
                       )
                   }):<h3>Empty cart</h3>}

                   TOTAL:{total}
                   <div className="pop_up_bottom">
                   <button className="popup__button"type="button" onClick={Checkout}>checkout</button>
                    <button className="popup__button"type="button" onClick={()=>setPop(!pop)}>Home</button>
                   </div>
                </div>
            </div>
    )
}

export default PopupTest
