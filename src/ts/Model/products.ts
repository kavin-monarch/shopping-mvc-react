export let products:Array<product_schema>=[
    {
    "product_id":12,
    "product_image":'https://images.unsplash.com/photo-1505740420928-5e560c06d30e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cHJvZHVjdHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
    "product_name":'Headset',
    "product_price":599
    },
    {
    "product_id":23,
    "product_image":'https://www.cloudways.com/blog/wp-content/uploads/portable-blender.jpg',
    "product_name":'Water bottle',
    "product_price":90
    },
    {
    "product_id":32,
    "product_image":'https://images-na.ssl-images-amazon.com/images/I/71E5zB1qbIL._SL1500_.jpg',
    "product_name":'Iphone 12 (128GB)',
    "product_price":65999
    },
    {
    "product_id":45,
    "product_image":'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
    "product_name":'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
    "product_price":1009
    },
    {
    "product_id": 2,
    "product_name": "Mens Casual Premium Slim Fit T-Shirts ",
    "product_price": 2222,
    "product_image": "https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg"
    },
    {
    "product_id": 3,
    "product_name": "Mens Cotton Jacket",
    "product_price": 5534,
    "product_image": "https://fakestoreapi.com/img/71li-ujtlUL._AC_UX679_.jpg"
    },

    {
    "product_id": 4,
    "product_name": "Mens Casual Slim Fit",
    "product_price": 1512,
    "product_image": "https://fakestoreapi.com/img/71YXzeOuslL._AC_UY879_.jpg"
    },

    {
    "product_id": 5,
    "product_name": "John Hardy Women's Legends Naga Gold & Silver Dragon Station Chain Bracelet",
    "product_price": 6952,
    "product_image": "https://fakestoreapi.com/img/71pWzhdJNwL._AC_UL640_QL65_ML3_.jpg"
    },

    {
    "product_id": 6,
    "product_name": "Solid Gold Petite Micropave ",
    "product_price": 1681,
    "product_image": "https://fakestoreapi.com/img/61sbMiUnoGL._AC_UL640_QL65_ML3_.jpg"
    },

    {
    "product_id": 7,
    "product_name": "White Gold Plated Princess",
    "product_price": 9155,
    "product_image": "https://fakestoreapi.com/img/71YAIFU48IL._AC_UL640_QL65_ML3_.jpg"
    },

    {
    "product_id": 8,
    "product_name": "Pierced Owl Rose Gold Plated Stainless Steel Double",
    "product_price": 1022,
    "product_image": "https://fakestoreapi.com/img/51UDEzMJVpL._AC_UL640_QL65_ML3_.jpg"
    },

    {
    "product_id": 9,
    "product_name": "WD 2TB Elements Portable External Hard Drive - USB 3.0 ",
    "product_price": 6423,
    "product_image": "https://fakestoreapi.com/img/61IBBVJvSDL._AC_SY879_.jpg"
    },

    {
    "product_id": 10,
    "product_name": "SanDisk SSD PLUS 1TB Internal SSD - SATA III 6 Gb/s",
    "product_price": 10923,
    "product_image": "https://fakestoreapi.com/img/61U7T1koQqL._AC_SX679_.jpg"
    },

    {
    "product_id": 11,
    "product_name": "Silicon Power 256GB SSD 3D NAND A55 SLC Cache Performance Boost SATA III 2.5",
    "product_price": 3394,
    "product_image": "https://fakestoreapi.com/img/71kWymZ+c+L._AC_SX679_.jpg"
    },

    {
    "product_id": 12,
    "product_name": "WD 4TB Gaming Drive Works with Playstation 4 Portable External Hard Drive",
    "product_price": 13314,
    "product_image": "https://fakestoreapi.com/img/61mtL65D4cL._AC_SX679_.jpg"
    },

    {
    "product_id": 13,
    "product_name": "Acer SB220Q bi 21.5 inches Full HD (1920 x 1080) IPS Ultra-Thin",
    "product_price": 5399,
    "product_image": "https://fakestoreapi.com/img/81QpkIctqPL._AC_SX679_.jpg"
    },

    {
    "product_id": 14,
    "product_name": "Samsung 49-Inch CHG90 144Hz Curved Gaming Monitor (LC49HG90DMNXZA) – Super Ultrawide Screen QLED ",
    "product_price": 9939,
    "product_image": "https://fakestoreapi.com/img/81Zt42ioCgL._AC_SX679_.jpg"
    },

    {
    "product_id": 15,
    "product_name": "BIYLACLESEN Women's 3-in-1 Snowboard Jacket Winter Coats",
    "product_price": 5643,
    "product_image": "https://fakestoreapi.com/img/51Y5NI-I5jL._AC_UX679_.jpg"
    },

    {
    "product_id": 16,
    "product_name": "Lock and Love Women's Removable Hooded Faux Leather Moto Biker Jacket",
    "product_price": 2943,
    "product_image": "https://fakestoreapi.com/img/81XH0e8fefL._AC_UY879_.jpg"
    },

    {
    "product_id": 17,
    "product_name": "Rain Jacket Women Windbreaker Striped Climbing Raincoats",
    "product_price": 3935,
    "product_image": "https://fakestoreapi.com/img/71HblAHs5xL._AC_UY879_-2.jpg"
    }
    
]