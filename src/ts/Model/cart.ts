export const user =(()=>{
    let cart:any[]=[];
    const addToCart =(product:product_schema):void=>{
        let flag:boolean=false;
        cart.map((item)=>{
            if(item.product_id===product.product_id){
                item.product_count++;
                flag=true;
                return item;
            }
            return item;
        })
        if(flag===false){
            let temp:cart_schema={
                product_id:product.product_id,
                product_image:product.product_image,
                product_name:product.product_name,
                product_price:product.product_price,
                product_count:1
            }
            cart.push(temp);
        }
    }

    const getCart =():Array<cart_schema>=>{
        return cart;
    }
    const update =(newCart:Array<cart_schema> | any[]):void=>{
        cart=newCart;
    }
    const add_reduce=(select:number,index:number):void=>{
        if(select===-1){
            getCart()[index].product_count>0 && getCart()[index].product_count--;
            if(getCart()[index].product_count===0){
                let newArray=getCart().filter((item,i)=>{
                    if(i!==index){
                        return item;
                    }
                    return '';
                });
                update(newArray);
            }
        }
        else if(select===0){
            let newArray=getCart().filter((item,i)=>{
                if(i!==index){
                    return item;
                }
                return null;
            });
            console.log(newArray);
            update(newArray);
        }
        else
        getCart()[index].product_count>0 && getCart()[index].product_count++;

    }

    return {
        addToCart:addToCart,
        getCart:getCart,
        update:update,
        add_reduce:add_reduce,
    }

})();
