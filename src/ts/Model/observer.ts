import {user} from '../Model/cart'

class CartManager{
    private observers:cartObserver[]=[];
    private prev=0;
    CartManager(){
        this.prev=user.getCart.length;
    }

    public attach(observer:cartObserver){
        let flag=0;
        this.observers.map(item=>{
            if(item===observer){
                flag=1;
            }
            return item;
        })
        if(flag===0){
            this.observers.push(observer);
        }
    }

    public detach(observer:cartObserver){
        this.observers=this.observers.filter(cart=>cart!==observer);
    }

    public updateCart(){
        const cart_length=user.getCart().length;
        console.log(this.prev+''+cart_length);
            if(this.prev!==cart_length){
                this.notify(cart_length);
                this.prev=cart_length;
            }
    }

    public notify(cart:number){
        this.observers.forEach(observer=>observer(cart));
    }
}

const cartManager =new CartManager();
export default cartManager;